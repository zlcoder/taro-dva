/**
 * 通过 webpack 的 require.context 进行自动获取 model 文件夹下的 model 文件
 * warning：应保持文件名称和 model 的 namespace 保持一致
 * @author nissen
 */
const flatten = arr => {
  return arr.reduce((result, item) => {
    return result.concat(Array.isArray(item) ? flatten(item) : item);
  }, []);
};

const context = require.context("./", true, /\.js$/);

const keys = context.keys().filter(item => item !== "./index.js");

const models = [];
for (let i = 0; i < keys.length; i += 1) {
  models.push(context(keys[i]));
}

export default flatten(models).map(model => model.default);
