/**
 * counter.js
 *
 * @author nissen
 * @version 2020.07.16
 * @description
 */
// import { loginIn, loginOut } from './service';
// import { ifSuccess, setToken } from '../../utils/utils';

export default {
  namespace: "counter",

  state: {
    count: 0
  },

  effects: {},

  reducers: {
    updateCount(state, { payload }) {
      const { count } = state;
      return {
        ...state,
        count: payload === "add" ? count + 1 : count - 1
      };
    }
  }
};
