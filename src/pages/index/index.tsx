import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, Text, View } from "@tarojs/components";
import Layout from "../../component/layout/index";
import "./index.less";

interface IProps {
  count: number;
  dispatch: any;
}

@connect(({ counter }) => ({ ...counter }))
export default class Index extends Component<IProps> {
  changeCount = (action: string): void => {
    this.props.dispatch({
      type: "counter/updateCount",
      payload: action
    });
  };

  render() {
    const { count } = this.props;

    return (
      <Layout>
        <View className="index">
          <Button className="add_btn" onClick={() => this.changeCount("add")}>
            +
          </Button>
          <Button className="dec_btn" onClick={() => this.changeCount("dec")}>
            -
          </Button>
          <View>
            <Text>{count}</Text>
          </View>
          <View>
            <Text>Hello, World</Text>
          </View>
        </View>
      </Layout>
    );
  }
}
