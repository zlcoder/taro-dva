import React, { FC, memo } from "react";
import { View } from "@tarojs/components";

/**
 *
 * @author nissen
 * @version 2020.07.16
 * @description
 */
interface LayoutProps {
  children: React.ReactNode;
}

const Layout: FC<LayoutProps> = (props: LayoutProps) => {
  const { children } = props;
  console.log(`children`, children);
  return <View>{children}</View>;
};

export default memo(Layout);
